<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>MainReaderWindow</name>
    <message>
        <source>TOC</source>
        <translation>Зміст</translation>
    </message>
    <message>
        <source>ISBN</source>
        <translation>ISBN</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Вихід</translation>
    </message>
    <message>
        <source>fast</source>
        <translation>Швидко</translation>
    </message>
    <message>
        <source>slow</source>
        <translation>Повільно</translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation>Аудіо плеєр</translation>
    </message>
    <message>
        <source>error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <source>vfast</source>
        <translation>Дуже швидко</translation>
    </message>
    <message>
        <source>vslow</source>
        <translation>Дуже повільно</translation>
    </message>
    <message>
        <source>Add Bookmark</source>
        <translation>Додати закладку</translation>
    </message>
    <message>
        <source>musicplaying</source>
        <translation>Будь ласка, зупиніть плеєр</translation>
    </message>
    <message>
        <source>File size</source>
        <translation>Розмір файлу</translation>
    </message>
    <message>
        <source>File type</source>
        <translation>Тип файлу</translation>
    </message>
    <message>
        <source>radioplaying</source>
        <translation>Будь ласка, зупиніть FM-радіо</translation>
    </message>
    <message>
        <source>Already the last page!</source>
        <translation>Це остання сторінка</translation>
    </message>
    <message>
        <source>Auto Next Page</source>
        <translation>Автоматичне гортання сторінок</translation>
    </message>
    <message>
        <source>openfailed</source>
        <translation>Не вдалось відкрити</translation>
    </message>
    <message>
        <source>Reflow</source>
        <translation>За розміром екрану</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Пошук тексту</translation>
    </message>
    <message>
        <source>Zoom Page</source>
        <translation>Параметри сторінки</translation>
    </message>
    <message>
        <source>BookMark</source>
        <translation>Закладки</translation>
    </message>
    <message>
        <source>Book Information</source>
        <translation>Інформація</translation>
    </message>
    <message>
        <source>Formated</source>
        <translation>Оригінальний формат</translation>
    </message>
    <message>
        <source>Close TTS</source>
        <translation>Зупинити читання вголос</translation>
    </message>
    <message>
        <source>Open TTS</source>
        <translation>Читання вголос</translation>
    </message>
    <message>
        <source>Already the first page!</source>
        <translation>Це перша сторінка книги.</translation>
    </message>
    <message>
        <source>normal</source>
        <translation>Нормально</translation>
    </message>
    <message>
        <source>Delete Bookmark</source>
        <translation>Видалити закладку</translation>
    </message>
    <message>
        <source>inputearphone</source>
        <translation>Читання вголос</translation>
    </message>
    <message>
        <source>Open File Failed!</source>
        <translation>Не вдалось відкрити файл.</translation>
    </message>
    <message>
        <source>Please insert earphone at first.</source>
        <translation>Будь ласка, приєднайте навушники.</translation>
    </message>
    <message>
        <source>Publisher</source>
        <translation>Видавець</translation>
    </message>
    <message>
        <source>TTS Setting</source>
        <translation>Налаштування читання вголос</translation>
    </message>
    <message>
        <source>chinese</source>
        <translation>Китайська</translation>
    </message>
    <message>
        <source>Add BookMark Failed!</source>
        <translation>Не вдалось додати закладку.</translation>
    </message>
    <message>
        <source>Delete BookMark Failed!</source>
        <translation>Не вдалось видалити закладку.</translation>
    </message>
    <message>
        <source>Go to Page</source>
        <translation>Перейти на сторінку</translation>
    </message>
    <message>
        <source>Gray Level</source>
        <translation>Рівень сірого</translation>
    </message>
    <message>
        <source>Delete BookMark Success!</source>
        <translation>Закладку видалено.</translation>
    </message>
    <message>
        <source>english</source>
        <translation>Англійська</translation>
    </message>
    <message>
        <source>Publish date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <source>Auto Page Setting</source>
        <translation>Налаштування автогортання</translation>
    </message>
    <message>
        <source>Advanced Line Spacing Settings</source>
        <translation>Відступ</translation>
    </message>
    <message>
        <source>Rotate Screen</source>
        <translation>Поворот сторінки</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Увага</translation>
    </message>
    <message>
        <source>warning</source>
        <translation>Увага</translation>
    </message>
    <message>
        <source>Search finished!</source>
        <translation>Пошук закінчено, більше не знайдено</translation>
    </message>
    <message>
        <source>AutoPage Setting</source>
        <translation>Автоматичне гортання</translation>
    </message>
    <message>
        <source>Advanced Zooms</source>
        <translation>Налаштування шрифту</translation>
    </message>
    <message>
        <source>Create TTS fail!</source>
        <translation>Не вдалось відкрити</translation>
    </message>
    <message>
        <source>Add BookMark Success!</source>
        <translation>Закладку збережено</translation>
    </message>
    <message>
        <source>View Bookmarks and Toc</source>
        <translation>Зміст і закладки</translation>
    </message>
    <message>
        <source>Can not find </source>
        <translation>Не вдалось знайти &quot;</translation>
    </message>
</context>
</TS>
