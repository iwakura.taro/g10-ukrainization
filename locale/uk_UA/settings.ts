<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>boeye::DrmActivateWidget</name>
    <message>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Вихід</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <source>Off the stacks</source>
        <translation>   </translation>
    </message>
    <message>
        <source>Off the stocks</source>
        <translation>   </translation>
    </message>
    <message>
        <source>Username:</source>
        <translation>Ім&apos;я користувача:</translation>
    </message>
    <message>
        <source>Drm Activate</source>
        <translation>Активація DRM</translation>
    </message>
</context>
<context>
    <name>boeye::PublicControlDialog</name>
    <message>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <source>open</source>
        <translation>Відкрити</translation>
    </message>
    <message>
        <source>Ukrainian</source>
        <translation>Українська</translation>
    </message>
    <message>
        <source>Czech</source>
        <translation>Czech</translation>
    </message>
    <message>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <source>close</source>
        <translation>Закрити </translation>
    </message>
    <message>
        <source>free:</source>
        <translation>Вільно: </translation>
    </message>
    <message>
        <source>Restore to Factory Settings</source>
        <translation>Відновити заводські налаштування</translation>
    </message>
    <message>
        <source>never</source>
        <translation>Вимкнено</translation>
    </message>
    <message>
        <source>Serial number</source>
        <translation>Серійний номер</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation>Italian</translation>
    </message>
    <message>
        <source>Display for Power On</source>
        <translation>Стартова сторінка</translation>
    </message>
    <message>
        <source>About Device</source>
        <translation>Про пристрій</translation>
    </message>
    <message>
        <source>Wifi connect failed!</source>
        <translation>Не вдалось приєднатись до мережі.</translation>
    </message>
    <message>
        <source>

Aa Bb Cc Dd Ee Ff Gg</source>
        <translation>Aa Bb Cc Dd Ee Ff Gg</translation>
    </message>
    <message>
        <source>Display for Boot</source>
        <translation>Показувати при завантаженні</translation>
    </message>
    <message>
        <source>no exist</source>
        <translation>Немає</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Ukrainian.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use English.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Select Display Type for Reader</source>
        <translation>Вибір виду перегляду в режимі читання</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Sanskrit.</source>
        <translation> </translation>
    </message>
    <message>
        <source>fullscreen</source>
        <translation>Повне оновлення екрану</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Romanian.</source>
        <translation> </translation>
    </message>
    <message>
        <source>unfullscreen</source>
        <translation>Не на повний екран </translation>
    </message>
    <message>
        <source>NauruLanguage</source>
        <translation>NauruLanguage</translation>
    </message>
    <message>
        <source>French</source>
        <translation>French</translation>
    </message>
    <message>
        <source>German</source>
        <translation>German</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation>Додати Wi-Fi мережу</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation>Polish</translation>
    </message>
    <message>
        <source>TaiWan</source>
        <translation>Traditional Chinese</translation>
    </message>
    <message>
        <source>Please enter the password to connect the network.</source>
        <translation>Введіть пароль:</translation>
    </message>
    <message>
        <source>Tcard Space</source>
        <translation>SD-карта</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Polish.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Esperanto</source>
        <translation>Esperanto</translation>
    </message>
    <message>
        <source>Restore Factory</source>
        <translation>Повернутися до заводських налаштувань</translation>
    </message>
    <message>
        <source>Software version</source>
        <translation>Версія ПЗ</translation>
    </message>
    <message>
        <source>minute</source>
        <translation>Хвилин (и)</translation>
    </message>
    <message>
        <source>public</source>
        <translation>Загальний</translation>
    </message>
    <message>
        <source>Hardware Address:</source>
        <translation>MAC адреса: </translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Finn.</source>
        <translation> </translation>
    </message>
    <message>
        <source>total:</source>
        <translation>Всього: </translation>
    </message>
    <message>
        <source>Wi-fi Connection</source>
        <translation>Wi-Fi мережі</translation>
    </message>
    <message>
        <source>Please press &quot;MENU&quot; for view</source>
        <translation>Натисніть кнопку &quot;Меню&quot; для перегляду</translation>
    </message>
    <message>
        <source>Sanskrit</source>
        <translation>Sanskrit</translation>
    </message>
    <message>
        <source>Battery</source>
        <translation>Заряд акумулятора</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Czech.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Password</source>
        <translation>пароль</translation>
    </message>
    <message>
        <source>Key Binding</source>
        <translation>Гарячі кнопки</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Swedish.</source>
        <translation> </translation>
    </message>
    <message>
        <source>LastReading</source>
        <translation>Остання прочитана сторінка</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use TaiWan.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Danish.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use NauruLanguage.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Сповіщення</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Russian.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation>Simplified Chinese</translation>
    </message>
    <message>
        <source>Flash Space</source>
        <translation>Пам&apos;ять</translation>
    </message>
    <message>
        <source>Select Reader for Epub</source>
        <translation>Програма читання EPUB</translation>
    </message>
    <message>
        <source>Check open software</source>
        <translation>Перелік компонентів</translation>
    </message>
    <message>
        <source>Could you want to restart?</source>
        <translation>Перезавантажити пристрій для зміни мови інтерфейсу?</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation>Робочий стіл</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Esperanto.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Chinese.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Question</source>
        <translation>Увага</translation>
    </message>
    <message>
        <source>Remind Time for Reading</source>
        <translation>Час нагадування</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Hungarian.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Bulgarian.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Screensaver</source>
        <translation>Заставка</translation>
    </message>
    <message>
        <source>Standby Time</source>
        <translation>Режим очікування</translation>
    </message>
    <message>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Portuguese.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Need to select at least one!</source>
        <translation>Виберіть хоча б одне зображення!</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Програма читання</translation>
    </message>
    <message>
        <source>Gravity Sensor</source>
        <translation>G-сенсор</translation>
    </message>
    <message>
        <source>E-ink batch</source>
        <translation>Екран</translation>
    </message>
    <message>
        <source>Update software version</source>
        <translation>Оновлення ПЗ</translation>
    </message>
    <message>
        <source>System Will Restart After Restore, Continue?</source>
        <translation>Перезавантажити пристрій, щоб застосувати зміни?</translation>
    </message>
    <message>
        <source>Fonts Manager</source>
        <translation>Шрифт</translation>
    </message>
    <message>
        <source>No Wifi Point</source>
        <translation>Wi-Fi мережу не знайдено</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use German.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use French.</source>
        <translation> </translation>
    </message>
    <message>
        <source>System Language</source>
        <translation>Інтерфейс</translation>
    </message>
    <message>
        <source>Applies to:
The language is the world&apos;s language,
As long as you can use Italian.</source>
        <translation> </translation>
    </message>
    <message>
        <source>Flush Settings</source>
        <translation>Режим оновлення екрану</translation>
    </message>
</context>
<context>
    <name>boeye::PwdWifiPointWidget</name>
    <message>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <source>Please Input Password:</source>
        <translation>Введіть пароль:</translation>
    </message>
</context>
<context>
    <name>boeye::SoftwareUpdateWidget</name>
    <message>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <source>Please update this software!</source>
        <translation>Є нова версія ПЗ.  Оновіть, будь ласка.</translation>
    </message>
    <message>
        <source>Testing software versions</source>
        <translation>Перевірка версії ПЗ</translation>
    </message>
    <message>
        <source>This software is latest version!</source>
        <translation>ПЗ останньої версії</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>Помилка!</translation>
    </message>
    <message>
        <source>Network disconnected!</source>
        <translation>Будь ласка, під&apos;єднайтесь до мережі</translation>
    </message>
    <message>
        <source>Please update software version!</source>
        <translation>Є нова версія ПЗ.  Оновіть, будь ласка.</translation>
    </message>
    <message>
        <source>Download failed!</source>
        <translation>Не вдалося завантажити файл.</translation>
    </message>
    <message>
        <source>Update successfully, Please restart!</source>
        <translation>Оновлено успішно, будь ласка, перезавантажте пристрій</translation>
    </message>
    <message>
        <source>Update failed!</source>
        <translation>Не вдалося оновити</translation>
    </message>
    <message>
        <source>Downloading...</source>
        <translation>Завантаження...</translation>
    </message>
    <message>
        <source>Updateing...</source>
        <translation>Оновлення...</translation>
    </message>
</context>
<context>
    <name>boeye::AddWifiPointWidget</name>
    <message>
        <source>Ok</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <source>WAP Encryption</source>
        <translation>WPA</translation>
    </message>
    <message>
        <source>Please enter the connection(SSID):</source>
        <translation>Будь ласка, введіть SSID мережі:</translation>
    </message>
    <message>
        <source>WEP Encryption</source>
        <translation>WEP</translation>
    </message>
    <message>
        <source>Please input SSID!</source>
        <translation>Будь ласка, введіть SSID мережі!</translation>
    </message>
    <message>
        <source>Please input password!</source>
        <translation>Будь ласка, введіть пароль</translation>
    </message>
    <message>
        <source>Up to 64 visible ASCII characters</source>
        <translation>До 64 символів</translation>
    </message>
    <message>
        <source>No Password</source>
        <translation>Загальнодоступна мережа</translation>
    </message>
    <message>
        <source>WAP2 Encryption</source>
        <translation>WPA2</translation>
    </message>
    <message>
        <source>Select the encryption method</source>
        <translation>Шифрування:</translation>
    </message>
</context>
<context>
    <name>boeye::SettingsMainWindow</name>
    <message>
        <source>DRM</source>
        <translation>DRM</translation>
    </message>
    <message>
        <source>WiFi</source>
        <translation>Безпровідні мережі</translation>
    </message>
    <message>
        <source>Fonts</source>
        <translation>Шрифт</translation>
    </message>
    <message>
        <source>Restore to Factory Settings</source>
        <translation>Повернутися до заводських налаштувань</translation>
    </message>
    <message>
        <source>never</source>
        <translation>Вимкнено</translation>
    </message>
    <message>
        <source>About Device</source>
        <translation>Про пристрій</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Від&apos;єднано</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation>Інструкція</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation>З&apos;єднано</translation>
    </message>
    <message>
        <source>minute</source>
        <translation>Хвилин</translation>
    </message>
    <message>
        <source>DateTime</source>
        <translation>Дата і час</translation>
    </message>
    <message>
        <source>Standby Time</source>
        <translation>Режим очікування</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Програма читання</translation>
    </message>
    <message>
        <source>Power Off</source>
        <translation>Вимкнути</translation>
    </message>
    <message>
        <source>System Will Restart After Restore, Continue?</source>
        <translation>Перезавантажити пристрій, щоб застосувати зміни?</translation>
    </message>
    <message>
        <source>Standby Picture</source>
        <translation>Заставка</translation>
    </message>
    <message>
        <source>System Language</source>
        <translation>Інтерфейс</translation>
    </message>
    <message>
        <source>Close All Programs and Shut Down the Device?</source>
        <translation>Зупинити всі програми і вимкнути пристрій?</translation>
    </message>
</context>
<context>
    <name>boeye::DateTimeWindow</name>
    <message>
        <source>Day</source>
        <translation>День</translation>
    </message>
    <message>
        <source>Hour</source>
        <translation>Година</translation>
    </message>
    <message>
        <source>Year</source>
        <translation>Рік</translation>
    </message>
    <message>
        <source>Month</source>
        <translation>Місяць</translation>
    </message>
    <message>
        <source>Use 12 hour style</source>
        <translation>12-годинний формат</translation>
    </message>
    <message>
        <source>Minute</source>
        <translation>Хвилина</translation>
    </message>
    <message>
        <source>Use network time</source>
        <translation>Використовувати час NTP</translation>
    </message>
</context>
<context>
    <name>boeye::FlushTimeWidget</name>
    <message>
        <source>BLWH</source>
        <translation>BLWS</translation>
    </message>
    <message>
        <source>FULL</source>
        <translation>Повне</translation>
    </message>
    <message>
        <source>LOCAL</source>
        <translation>Часткове</translation>
    </message>
    <message>
        <source>Flush Settings</source>
        <translation>Оновлення екрану</translation>
    </message>
</context>
<context>
    <name>boeye::DescWifiWidget</name>
    <message>
        <source>DNS:</source>
        <translation>DNS:</translation>
    </message>
    <message>
        <source>Current connection spots:</source>
        <translation>Під&apos;єднано до мережі:</translation>
    </message>
    <message>
        <source>Network Details</source>
        <translation>Інформація про мережу</translation>
    </message>
    <message>
        <source>Router address:</source>
        <translation>Адреса маршрутизатора:</translation>
    </message>
    <message>
        <source>IP address:</source>
        <translation>IP адреса:</translation>
    </message>
    <message>
        <source>Netmask:</source>
        <translation>Маска мережі:</translation>
    </message>
</context>
<context>
    <name>boeye::Menu</name>
    <message>
        <source>Connection</source>
        <translation>Під&apos;єднатися до мережі</translation>
    </message>
    <message>
        <source>Forgot</source>
        <translation>Видалити цю мережу</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Опис мережі</translation>
    </message>
    <message>
        <source>Scanning</source>
        <translation>Пошук мереж</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Від&apos;єднатись</translation>
    </message>
</context>
<context>
    <name>boeye::DrmactivatelistWidget</name>
    <message>
        <source>Username:</source>
        <translation>Ім&apos;я користувача:</translation>
    </message>
</context>
</TS>
