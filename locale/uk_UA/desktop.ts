<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>Calendar</name>
    <message>
        <source>Apr</source>
        <translation>Квітень</translation>
    </message>
    <message>
        <source>Aug</source>
        <translation>Серпень</translation>
    </message>
    <message>
        <source>Dec</source>
        <translation>Грудень</translation>
    </message>
    <message>
        <source>Feb</source>
        <translation>Лютий</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation>Пт</translation>
    </message>
    <message>
        <source>Jan</source>
        <translation>Січень</translation>
    </message>
    <message>
        <source>Jul</source>
        <translation>Липень</translation>
    </message>
    <message>
        <source>Jun</source>
        <translation>Червень</translation>
    </message>
    <message>
        <source>Mar</source>
        <translation>Березень</translation>
    </message>
    <message>
        <source>May</source>
        <translation>Травень</translation>
    </message>
    <message>
        <source>Mon</source>
        <translation>Пн</translation>
    </message>
    <message>
        <source>Nov</source>
        <translation>Листопад</translation>
    </message>
    <message>
        <source>Oct</source>
        <translation>Жовтень</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation>Сб</translation>
    </message>
    <message>
        <source>Sep</source>
        <translation>Вересень</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation>Нд</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation>Чт</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation>Вт</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation>Ср</translation>
    </message>
</context>
<context>
    <name>MainWin</name>
    <message>
        <source>Music Player</source>
        <translation>Плеєр</translation>
    </message>
    <message>
        <source>Current Reading</source>
        <translation>   Нещодавнi</translation>
    </message>
    <message>
        <source>Poweroff</source>
        <translation>Вимкнути</translation>
    </message>
    <message>
        <source>Shut down</source>
        <translation>Вимкнути</translation>
    </message>
    <message>
        <source>Do you want to shut down your device?</source>
        <translation>Вимкнути пристрій?</translation>
    </message>
    <message>
        <source>Applications</source>
        <translation>   Програми</translation>
    </message>
    <message>
        <source>Clear All Recent</source>
        <translation>Очистити історію</translation>
    </message>
    <message>
        <source>Delete Recent</source>
        <translation>Видалити</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>No recently reading</source>
        <translation>Не читалось...</translation>
    </message>
</context>
</TS>
