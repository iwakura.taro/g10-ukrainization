<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>QObject</name>
    <message>
        <source>Open zip</source>
        <translation>Відкрити zip</translation>
    </message>
</context>
<context>
    <name>ArcPasswordDialog</name>
    <message>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <source>Enter Password</source>
        <translation>Будь ласка, введіть пароль</translation>
    </message>
</context>
</TS>
