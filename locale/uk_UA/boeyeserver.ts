<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>USBUIManager</name>
    <message>
        <source>Connect to PC?</source>
        <translation>Приєднати до комп&apos;ютера?</translation>
    </message>
    <message>
        <source>USB Connection</source>
        <translation>USB зв&apos;язок</translation>
    </message>
</context>
<context>
    <name>BatteryUIManager</name>
    <message>
        <source>System is low-power, please charge the battery</source>
        <translation>Низький заряд акумулятора.  Будь ласка, зарядіть.</translation>
    </message>
    <message>
        <source>Low-Power Warning</source>
        <translation>Увага</translation>
    </message>
</context>
<context>
    <name>BoeyeQWSServer</name>
    <message>
        <source>Shut Down the ereader?</source>
        <translation>Вимкнути пристрій?</translation>
    </message>
    <message>
        <source>Shut Down</source>
        <translation>Завершення роботи</translation>
    </message>
</context>
</TS>
