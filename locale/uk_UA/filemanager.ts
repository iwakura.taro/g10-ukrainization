<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>component::BoeyeListView</name>
    <message>
        <source>bmp</source>
        <translation>BMP</translation>
    </message>
    <message>
        <source>chm</source>
        <translation>CHM</translation>
    </message>
    <message>
        <source>fb2</source>
        <translation>FB2</translation>
    </message>
    <message>
        <source>gif</source>
        <translation>GIF</translation>
    </message>
    <message>
        <source>htm</source>
        <translation>HTM</translation>
    </message>
    <message>
        <source>jpg</source>
        <translation>JPG</translation>
    </message>
    <message>
        <source>mp3</source>
        <translation>MP3</translation>
    </message>
    <message>
        <source>ogg</source>
        <translation>OGG</translation>
    </message>
    <message>
        <source>pdb</source>
        <translation>PDB</translation>
    </message>
    <message>
        <source>pdf</source>
        <translation>PDF</translation>
    </message>
    <message>
        <source>png</source>
        <translation>PNG</translation>
    </message>
    <message>
        <source>prc</source>
        <translation>PRC</translation>
    </message>
    <message>
        <source>rar</source>
        <translation>RAR</translation>
    </message>
    <message>
        <source>rtf</source>
        <translation>RTF</translation>
    </message>
    <message>
        <source>tcr</source>
        <translation>TCR</translation>
    </message>
    <message>
        <source>tif</source>
        <translation>TIF</translation>
    </message>
    <message>
        <source>wav</source>
        <translation>WAV</translation>
    </message>
    <message>
        <source>wma</source>
        <translation>WMA</translation>
    </message>
    <message>
        <source>zip</source>
        <translation>ZIP</translation>
    </message>
    <message>
        <source>ISBN</source>
        <translation>ISBN</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <source>Sort</source>
        <translation>Сортування</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <source>djvu</source>
        <translation>DJVU</translation>
    </message>
    <message>
        <source>html</source>
        <translation>HTML</translation>
    </message>
    <message>
        <source>jpeg</source>
        <translation>JPEG</translation>
    </message>
    <message>
        <source>mobi</source>
        <translation>Mobi</translation>
    </message>
    <message>
        <source>text</source>
        <translation>TEXT</translation>
    </message>
    <message>
        <source>tiff</source>
        <translation>TIFF</translation>
    </message>
    <message>
        <source>Jump to page</source>
        <translation>Перейти на сторінку</translation>
    </message>
    <message>
        <source>Sort by author</source>
        <translation>Сортувати за автором</translation>
    </message>
    <message>
        <source>Books</source>
        <translation>Книжки</translation>
    </message>
    <message>
        <source>Include</source>
        <translation>Містить</translation>
    </message>
    <message>
        <source>Flash</source>
        <translation>Внутрішня пам&apos;ять</translation>
    </message>
    <message>
        <source>Memos</source>
        <translation>Пам&apos;ятки</translation>
    </message>
    <message>
        <source>Music</source>
        <translation>Музика</translation>
    </message>
    <message>
        <source>TCard</source>
        <translation>Карта пам&apos;яті</translation>
    </message>
    <message>
        <source>Icon model</source>
        <translation>Обкладинки</translation>
    </message>
    <message>
        <source>Open music player</source>
        <translation>Плеєр</translation>
    </message>
    <message>
        <source>Are sure to clear all system bookmark?</source>
        <translation>Очистити історію читання?</translation>
    </message>
    <message>
        <source>Remove Bookmark</source>
        <translation>Видалення закладки</translation>
    </message>
    <message>
        <source>File size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <source>File type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <source> Files,</source>
        <translation>файл(и), </translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>Тека</translation>
    </message>
    <message>
        <source>Are sure to remove &quot;</source>
        <translation>Видалити &quot;</translation>
    </message>
    <message>
        <source>Pixels</source>
        <translation>Точок</translation>
    </message>
    <message>
        <source>Recent</source>
        <translation>Нещодавні</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Пошук файлу</translation>
    </message>
    <message>
        <source>No data exist in the directory!</source>
        <translation>Пусто</translation>
    </message>
    <message>
        <source>Properity</source>
        <translation>Інформація</translation>
    </message>
    <message>
        <source>List model</source>
        <translation>Перелік</translation>
    </message>
    <message>
        <source>folder</source>
        <translation>Тека</translation>
    </message>
    <message>
        <source> Subfolder(s)</source>
        <translation> об&apos;єктів</translation>
    </message>
    <message>
        <source>Last modified</source>
        <translation>Змінено</translation>
    </message>
    <message>
        <source>Clear bookmark</source>
        <translation>Увага</translation>
    </message>
    <message>
        <source>Publisher</source>
        <translation>Видавець</translation>
    </message>
    <message>
        <source>Create time</source>
        <translation>Дата створення</translation>
    </message>
    <message>
        <source>Create memo</source>
        <translation>Додати пам&apos;ятку</translation>
    </message>
    <message>
        <source>List mode</source>
        <translation>Перелік</translation>
    </message>
    <message>
        <source>microsoft document format</source>
        <translation>документ</translation>
    </message>
    <message>
        <source>All Pictures</source>
        <translation>Всі фотографії</translation>
    </message>
    <message>
        <source>Clear all</source>
        <translation>Видалити все</translation>
    </message>
    <message>
        <source>Album Name</source>
        <translation>Альбом</translation>
    </message>
    <message>
        <source>Sort by size</source>
        <translation>Сортувати за розміром</translation>
    </message>
    <message>
        <source>Sort by type</source>
        <translation>Сортувати за типом</translation>
    </message>
    <message>
        <source>Sort by time</source>
        <translation>Сортувати за часом створення</translation>
    </message>
    <message>
        <source>Sort by name</source>
        <translation>Сортувати за назвою</translation>
    </message>
    <message>
        <source>Remove File</source>
        <translation>Увага</translation>
    </message>
    <message>
        <source>Publish date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <source>Descending sort</source>
        <translation>Сортувати за спаданням</translation>
    </message>
    <message>
        <source>Ascending sort</source>
        <translation>Сортувати за зростанням</translation>
    </message>
    <message>
        <source> Folders</source>
        <translation> теки</translation>
    </message>
    <message>
        <source>All Books</source>
        <translation>Всі книжки</translation>
    </message>
    <message>
        <source>All Memos</source>
        <translation>Всі пам&apos;ятки</translation>
    </message>
    <message>
        <source>All Music</source>
        <translation>Вся музика</translation>
    </message>
    <message>
        <source>Pictures</source>
        <translation>Фотографії</translation>
    </message>
    <message>
        <source>EPUB document</source>
        <translation>EPUB</translation>
    </message>
</context>
<context>
    <name>component::BoeyeFileSystemModel</name>
    <message>
        <source>Name</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <source>Owner</source>
        <translation>Власник</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Увага</translation>
    </message>
    <message>
        <source>Can not open archive!</source>
        <translation>Не вдалось відкрити архів.</translation>
    </message>
    <message>
        <source>Date Modified</source>
        <translation>Дата зміни</translation>
    </message>
</context>
<context>
    <name>component::MainWindowfortest</name>
    <message>
        <source>card</source>
        <translation>Карта</translation>
    </message>
    <message>
        <source>books</source>
        <translation>Книжки</translation>
    </message>
    <message>
        <source>MainWindow</source>
        <translation>Головне вікно</translation>
    </message>
    <message>
        <source>musics</source>
        <translation>Музика</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Today</source>
        <translation>Сьогодні</translation>
    </message>
    <message>
        <source>Yesterday</source>
        <translation>Вчора</translation>
    </message>
</context>
<context>
    <name>component::FileManagerMainWindow</name>
    <message>
        <source>No TCard</source>
        <translation>Картку пам&apos;яті не знайдено.</translation>
    </message>
    <message>
        <source>No TCard was inserted, if you have inserted TCard, please reboot device!</source>
        <translation>Будь ласка, вставте картку пам&apos;яті.  Якщо вона вже встановлена - спробуйте перезавантажити пристрій.</translation>
    </message>
</context>
</TS>
