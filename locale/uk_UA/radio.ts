<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>inputFreqDlg</name>
    <message>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Input Frequency</source>
        <translation>Введіть частоту</translation>
    </message>
    <message>
        <source>warning</source>
        <translation>попередження </translation>
    </message>
</context>
<context>
    <name>saveChannelDlg</name>
    <message>
        <source>FM:</source>
        <translation>Частота FM </translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Редагувати </translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Зберегти станцію </translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Назва станції:</translation>
    </message>
    <message>
        <source>Sort:</source>
        <translation>Назва станції:</translation>
    </message>
    <message>
        <source>Sport channel</source>
        <translation>Спорт</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Music channel</source>
        <translation>Музика</translation>
    </message>
    <message>
        <source>Chat channel</source>
        <translation>Балачки</translation>
    </message>
    <message>
        <source>warning</source>
        <translation>попередження </translation>
    </message>
    <message>
        <source>Traffic channel</source>
        <translation>Трафік</translation>
    </message>
    <message>
        <source>Other channel</source>
        <translation>Інше</translation>
    </message>
    <message>
        <source>Channel name</source>
        <translation>Назва станції</translation>
    </message>
</context>
<context>
    <name>boeyeradioui</name>
    <message>
        <source>Edit</source>
        <translation>Редагувати</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation> Видалити</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation>Гучність</translation>
    </message>
    <message>
        <source>Fine Tuning</source>
        <translation>тонка настройка</translation>
    </message>
    <message>
        <source>please insert earphone at first.</source>
        <translation>Під&apos;єднайте навушники, будь ласка</translation>
    </message>
    <message>
        <source>You should close music player firstly.</source>
        <translation>Зупиніть плеєр</translation>
    </message>
    <message>
        <source>You should close TTS firstly.</source>
        <translation>Зупиніть читання</translation>
    </message>
    <message>
        <source>Are you sure to delete the channel?</source>
        <translation>Видалити станцію?</translation>
    </message>
    <message>
        <source>Input frequency</source>
        <translation>Введіть частоту</translation>
    </message>
    <message>
        <source>Save Channel</source>
        <translation>Зберегти станцію</translation>
    </message>
    <message>
        <source>warning</source>
        <translation>попередження</translation>
    </message>
</context>
<context>
    <name>quitDlg</name>
    <message>
        <source>Quit</source>
        <translation>вихід </translation>
    </message>
    <message>
        <source>Quit the application</source>
        <translation>Вийти</translation>
    </message>
    <message>
        <source>Play background</source>
        <translation>Фонове звучання</translation>
    </message>
    <message>
        <source>warning</source>
        <translation>попередження </translation>
    </message>
</context>
<context>
    <name>setVolumeDlg</name>
    <message>
        <source>Volume</source>
        <translation>Гучність</translation>
    </message>
    <message>
        <source>warning</source>
        <translation>попередження</translation>
    </message>
</context>
<context>
    <name>adjustDlg</name>
    <message>
        <source>Fine Tuning</source>
        <translation> тонка настройка</translation>
    </message>
    <message>
        <source>warning</source>
        <translation>попередження </translation>
    </message>
</context>
</TS>
