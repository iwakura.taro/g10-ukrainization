<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>boeye::Downloader</name>
    <message>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <source>Downloading </source>
        <translation>Завантажую...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
</context>
<context>
    <name>boeye::Menu</name>
    <message>
        <source>Zoom in</source>
        <translation>Збільшити</translation>
    </message>
    <message>
        <source>Input URL address</source>
        <translation>Ввести адресу</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <source>Go to home page</source>
        <translation>Домашня сторінка</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>Переіменувати</translation>
    </message>
    <message>
        <source>Zoom out</source>
        <translation>Зменшити</translation>
    </message>
    <message>
        <source>Move to the top</source>
        <translation>Перейти вгору</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <source>Set home page</source>
        <translation>Встановити домашню сторінку</translation>
    </message>
    <message>
        <source>Add to address book</source>
        <translation>Додати до адресної книги</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <source>Address book</source>
        <translation>Адресна книга</translation>
    </message>
    <message>
        <source>Full screen display</source>
        <translation>Повноекранний режим</translation>
    </message>
    <message>
        <source>Unfull screen display</source>
        <translation>Звичайний режим</translation>
    </message>
</context>
<context>
    <name>boeye::Window</name>
    <message>
        <source>Set Homepage Successfully!</source>
        <translation>Домашню сторінку збережено.</translation>
    </message>
    <message>
        <source>Set BookMark successfully!</source>
        <translation>Закладку збережено.</translation>
    </message>
    <message>
        <source>This bookmark is exist!</source>
        <translation>Ця закладка вже є!</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Повідомлення</translation>
    </message>
</context>
<context>
    <name>boeye::OptionsWindow</name>
    <message>
        <source>Enable image</source>
        <translation>Показувати зображення</translation>
    </message>
    <message>
        <source>Disable cookie</source>
        <translation>Заборонити cookies</translation>
    </message>
    <message>
        <source>Disable encoding detect</source>
        <translation>Не визначати кодування</translation>
    </message>
    <message>
        <source>Enable encoding detect</source>
        <translation>Визначати кодування</translation>
    </message>
    <message>
        <source>Disable plugin</source>
        <translation>Заборонити додатки</translation>
    </message>
    <message>
        <source>Enable javascript</source>
        <translation>Дозволити JavaScript</translation>
    </message>
    <message>
        <source>Enable cookie</source>
        <translation>Дозволити cookies</translation>
    </message>
    <message>
        <source>Enable plugin</source>
        <translation>Дозволити додатки</translation>
    </message>
    <message>
        <source>Disable image</source>
        <translation>Заборонити показ зображення</translation>
    </message>
    <message>
        <source>Disable javascript</source>
        <translation>Заборонити JavaScript</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Unknown title</source>
        <translation>Назва відсутня</translation>
    </message>
    <message>
        <source>The file is not an XBEL version 1.0 file.</source>
        <translation>Це не файл XBEL version 1.0.</translation>
    </message>
</context>
<context>
    <name>boeye::AutoFillManager</name>
    <message>
        <source>Not now</source>
        <translation>Не зараз</translation>
    </message>
    <message>
        <source>Never for this site</source>
        <translation>Ніколи не зберігати файли з цього сайту</translation>
    </message>
    <message>
        <source>&lt;b&gt;Would you like to save this password?&lt;/b&gt;&lt;br&gt; To review passwords you have saved and remove them, open the AutoFill panel of preferences.</source>
        <translation>&lt;b&gt;Зберегти пароль?&lt;/b&gt;&lt;br&gt; Для перегляду або видалення збережених паролів, використайте панель автозаповнення.</translation>
    </message>
</context>
</TS>
