<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>ui::RForrestGotoWidget</name>
    <message>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <source>Del</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <source>Goto Page</source>
        <translation>Перейти на сторінку</translation>
    </message>
</context>
<context>
    <name>ui::RItemBox</name>
    <message>
        <source>Gray</source>
        <translation>Рівень сірого</translation>
    </message>
    <message>
        <source>Rotate</source>
        <translation>Поворот сторінки</translation>
    </message>
    <message>
        <source>Contrast</source>
        <translation>Контрастність</translation>
    </message>
    <message>
        <source>Adapted</source>
        <translation>Адаптований</translation>
    </message>
</context>
<context>
    <name>ui::RAnnoWidget</name>
    <message>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Очистити</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
</context>
<context>
    <name>ui::RBoxGroup</name>
    <message>
        <source>Zoom</source>
        <translation>Параметри сторінки</translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation>Розмір шрифту</translation>
    </message>
    <message>
        <source>Adapated Screen</source>
        <translation>Налаштування перегляду</translation>
    </message>
</context>
<context>
    <name>ui::InputDialog</name>
    <message>
        <source>Jump to Page</source>
        <translation>Перейти на сторінку</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <source>Please enter the book&apos;s name.</source>
        <translation>Будь ласка, введіть назву</translation>
    </message>
    <message>
        <source>Please enter the search string.</source>
        <translation>Будь ласка, введіть текст</translation>
    </message>
    <message>
        <source>Please enter the page number.</source>
        <translation>Будь ласка, введіть номер сторінки</translation>
    </message>
    <message>
        <source>Search File</source>
        <translation>Пошук</translation>
    </message>
</context>
<context>
    <name>ui::RForrestBookinfoWindow</name>
    <message>
        <source>ISBN:</source>
        <translation>ISBN:</translation>
    </message>
    <message>
        <source>Publisher:</source>
        <translation>Видавець:</translation>
    </message>
    <message>
        <source>My Rating:</source>
        <translation>Оцінка:</translation>
    </message>
    <message>
        <source>Summary:</source>
        <translation>Резюме:</translation>
    </message>
    <message>
        <source>Publish Date:</source>
        <translation>Дата публікації:</translation>
    </message>
</context>
<context>
    <name>ui::RBookinfoWidget</name>
    <message>
        <source>Size:</source>
        <translation>Розмір файлу:</translation>
    </message>
    <message>
        <source>Where:</source>
        <translation>Aдреса книги:</translation>
    </message>
    <message>
        <source>Rating:</source>
        <translation>Оцінка:</translation>
    </message>
    <message>
        <source>Author:</source>
        <translation>Автор:</translation>
    </message>
    <message>
        <source>Modified:</source>
        <translation>Змінено:</translation>
    </message>
    <message>
        <source>Summary:</source>
        <translation>Резюме:</translation>
    </message>
    <message>
        <source>About Book</source>
        <translation>Інформація</translation>
    </message>
    <message>
        <source>Book Name:</source>
        <translation>Назва:</translation>
    </message>
</context>
<context>
    <name>ui::RFontWidget</name>
    <message>
        <source>Line Spacing Advance</source>
        <translation>Налаштування відстані між рядками</translation>
    </message>
    <message>
        <source>Font Size Advance</source>
        <translation>Налаштування розміру шрифту</translation>
    </message>
    <message>
        <source>Margins</source>
        <translation>Поля</translation>
    </message>
    <message>
        <source>Use this setting for other books</source>
        <translation>Задіяти поточні налаштування для всіх книг</translation>
    </message>
    <message>
        <source>Font Family</source>
        <translation>Шрифти</translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation>Розмір шрифту</translation>
    </message>
    <message>
        <source>Font Style</source>
        <translation>Стиль шрифту</translation>
    </message>
    <message>
        <source>Publisher Default</source>
        <translation>Налаштування видавця</translation>
    </message>
    <message>
        <source>Line Spacing</source>
        <translation>Міжрядковий інтервал</translation>
    </message>
    <message>
        <source>Margins Advance</source>
        <translation>Налаштування полів</translation>
    </message>
</context>
<context>
    <name>ui::ReaderWidget</name>
    <message>
        <source>Line Spacing Advance</source>
        <translation>Налаштування міжрядкового інтервалу</translation>
    </message>
    <message>
        <source>Font Size Advance</source>
        <translation>Налаштування розміру шрифту</translation>
    </message>
    <message>
        <source>Margins Advance</source>
        <translation>Налаштування полів</translation>
    </message>
</context>
<context>
    <name>ui::Memo</name>
    <message>
        <source>Sorry, error occurred when saving memo.</source>
        <translation>Помилка при збереженні пам&apos;ятки </translation>
    </message>
    <message>
        <source>Fail to save memo: no space left on device.</source>
        <translation>Помилка: немає вільного місця</translation>
    </message>
    <message>
        <source>Save memo</source>
        <translation>Зберегти пам&apos;ятку </translation>
    </message>
    <message>
        <source>new memo</source>
        <translation>Нова пам&apos;ятка</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Увага</translation>
    </message>
    <message>
        <source>Fail to open file</source>
        <translation>Не вдалось відкрити файл</translation>
    </message>
</context>
<context>
    <name>ui::RCheckBoxGroup</name>
    <message>
        <source>Hide bottom bar</source>
        <translation>Сховати панель статусу</translation>
    </message>
    <message>
        <source>Use this setting for other books</source>
        <translation>Використовувати поточні налаштування для всіх книг</translation>
    </message>
    <message>
        <source>Publisher Default</source>
        <translation>Налаштування видавця</translation>
    </message>
</context>
<context>
    <name>ui::RTtsWidget</name>
    <message>
        <source>TTS Reading Speed</source>
        <translation>Швидкість читання</translation>
    </message>
    <message>
        <source>TTS Language</source>
        <translation>Мова</translation>
    </message>
    <message>
        <source>TTS Volume</source>
        <translation>Гучність читання</translation>
    </message>
</context>
<context>
    <name>ui::BasicInfoWidget</name>
    <message>
        <source>BASIC INFORMATION</source>
        <translation>Інформація</translation>
    </message>
    <message>
        <source>Where:</source>
        <translation>Розташування:</translation>
    </message>
    <message>
        <source>SUMMARY</source>
        <translation>Резюме</translation>
    </message>
</context>
<context>
    <name>ui::RForrestView</name>
    <message>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <source>No data exist!</source>
        <translation>Немає данних</translation>
    </message>
    <message>
        <source>Delete All</source>
        <translation>Видалити все</translation>
    </message>
</context>
</TS>
