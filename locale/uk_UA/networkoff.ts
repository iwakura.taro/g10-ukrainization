<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>QObject</name>
    <message>
        <source>Do you want to disconnect from the network?</source>
        <translation>Від&apos;єднатись від мережі?</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Безпровідна мережа</translation>
    </message>
</context>
</TS>
