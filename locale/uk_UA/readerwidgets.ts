<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>ui::RForrestGotoWidget</name>
    <message>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <source>Del</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <source>Goto Page</source>
        <translation>Перейти на сторінку</translation>
    </message>
</context>
<context>
    <name>ui::RAnnoWidget</name>
    <message>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Очистити</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
</context>
<context>
    <name>ui::RBoxGroup</name>
    <message>
        <source>Zoom</source>
        <translation>Параметри сторінки</translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation>Розмір шрифту</translation>
    </message>
    <message>
        <source>Adapated Screen</source>
        <translation>По розміру екрану</translation>
    </message>
</context>
<context>
    <name>ui::RForrestBookinfoWindow</name>
    <message>
        <source>ISBN:</source>
        <translation>ISBN:</translation>
    </message>
    <message>
        <source>Publisher:</source>
        <translation>Видавець:</translation>
    </message>
    <message>
        <source>My Rating:</source>
        <translation>Оцінка:</translation>
    </message>
    <message>
        <source>Summary:</source>
        <translation>Резюме:</translation>
    </message>
    <message>
        <source>Publish Date:</source>
        <translation>Дата публікації:</translation>
    </message>
</context>
<context>
    <name>ui::RBookinfoWidget</name>
    <message>
        <source>Size:</source>
        <translation>Розмір файлу:</translation>
    </message>
    <message>
        <source>Where:</source>
        <translation>Адреса книги:</translation>
    </message>
    <message>
        <source>Rating:</source>
        <translation>Оцінка:</translation>
    </message>
    <message>
        <source>Author:</source>
        <translation>Автор:</translation>
    </message>
    <message>
        <source>Modified:</source>
        <translation>Змінено:</translation>
    </message>
    <message>
        <source>Summary:</source>
        <translation>Резюме:</translation>
    </message>
    <message>
        <source>About Book</source>
        <translation>Інформація про книгу</translation>
    </message>
    <message>
        <source>Book Name:</source>
        <translation>Назва:</translation>
    </message>
</context>
<context>
    <name>ui::RFontWidget</name>
    <message>
        <source>Line Spacing Advance</source>
        <translation>Розширені налаштування міжрядкового інтервалу</translation>
    </message>
    <message>
        <source>Font Size Advance</source>
        <translation>Розмір шрифту</translation>
    </message>
    <message>
        <source>Margins</source>
        <translation>Поля</translation>
    </message>
    <message>
        <source>Font Family</source>
        <translation>Шрифти</translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation>Розмір шрифту</translation>
    </message>
    <message>
        <source>Font Style</source>
        <translation>Стиль шрифту</translation>
    </message>
    <message>
        <source>Line Spacing</source>
        <translation>Міжрядковий інтервал</translation>
    </message>
    <message>
        <source>Margins Advance</source>
        <translation>Налаштування полів</translation>
    </message>
</context>
<context>
    <name>ui::ReaderWidget</name>
    <message>
        <source>Line Spacing Advance</source>
        <translation>Міжрядковий інтервал</translation>
    </message>
    <message>
        <source>Font Size Advance</source>
        <translation>Розмір шрифту</translation>
    </message>
    <message>
        <source>Margins Advance</source>
        <translation>Налаштування полів</translation>
    </message>
</context>
<context>
    <name>ui::RTtsWidget</name>
    <message>
        <source>TTS Reading Speed</source>
        <translation>Швидкість читання</translation>
    </message>
    <message>
        <source>TTS Language</source>
        <translation>Мова читання</translation>
    </message>
    <message>
        <source>TTS Volume</source>
        <translation>Гучність читання</translation>
    </message>
</context>
<context>
    <name>ui::BasicInfoWidget</name>
    <message>
        <source>BASIC INFORMATION</source>
        <translation>Інформація про книгу</translation>
    </message>
    <message>
        <source>Where:</source>
        <translation>Файл:</translation>
    </message>
    <message>
        <source>SUMMARY</source>
        <translation>Резюме</translation>
    </message>
</context>
<context>
    <name>ui::RForrestView</name>
    <message>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <source>No data exist!</source>
        <translation>Немає даних</translation>
    </message>
    <message>
        <source>Delete All</source>
        <translation>Видалити все</translation>
    </message>
</context>
<context>
    <name>ui::RItemBox</name>
    <message>
        <source>Rotate</source>
        <translation>Поворот сторінки</translation>
    </message>
    <message>
        <source>Contrast</source>
        <translation>Контрастність</translation>
    </message>
    <message>
        <source>Adapted</source>
        <translation>Адаптований</translation>
    </message>
</context>
</TS>
