<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>QStarDict::SetDicts</name>
    <message>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>Дозволено</translation>
    </message>
</context>
<context>
    <name>QStarDict::SetDictsDialog</name>
    <message>
        <source>QStarDict Settings</source>
        <translation>Налаштування QStarDict</translation>
    </message>
    <message>
        <source>Tip:&apos;up or down arrows&apos; to select; &apos;Enter&apos; to change; &apos;Escape&apos;  to cancel and exit</source>
        <translation>Дія: &quot;вгору або вниз&quot; - щоб вибрати, &quot;Enter&quot; - щоб змінити, &quot;Назад&quot; - скасувати</translation>
    </message>
    <message>
        <source>Dictionaries Settings</source>
        <translation>Налаштування словника</translation>
    </message>
</context>
<context>
    <name>QStarDict::DictMainWindow</name>
    <message>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <source>QStarDict</source>
        <translation>QStarDict</translation>
    </message>
    <message>
        <source>Dictionary</source>
        <translation>Словник</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
</context>
<context>
    <name>QStarDict::DictBrowser</name>
    <message>
        <source>No dictionary is selected!</source>
        <translation>Оберіть cловник!</translation>
    </message>
    <message>
        <source>The word &lt;b&gt;%1&lt;/b&gt; is not found.</source>
        <translation>Слово &lt;b&gt;%1&lt;/b&gt; не знайдено</translation>
    </message>
</context>
<context>
    <name>DictBrowserSearch</name>
    <message>
        <source>Search:</source>
        <translation>Пошук:</translation>
    </message>
</context>
</TS>
