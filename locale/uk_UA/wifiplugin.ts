<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>ui::WiFiPlugIn</name>
    <message>
        <source>More</source>
        <translation>Інші</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <source>Failed to start Wi-Fi!</source>
        <translation>Не вдалось запустити Wi-Fi</translation>
    </message>
    <message>
        <source>Incorrect password!</source>
        <translation>Неправильний пароль.  Спробуйте ще раз.</translation>
    </message>
    <message>
        <source>Failed to scan!</source>
        <translation>Не вдалось здійснити пошук мереж</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation>Перевірка</translation>
    </message>
    <message>
        <source>Failed to disconnect!</source>
        <translation>Не вдалось від&apos;єднатися від мережі.</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation>З&apos;єднано</translation>
    </message>
    <message>
        <source>Sorry, something wrong now.Retry please!</source>
        <translation>Сталася помилка.  Повторіть дію.</translation>
    </message>
    <message>
        <source>Please input password.</source>
        <translation>Будь ласка, введіть пароль для під&apos;єднання до мережі</translation>
    </message>
    <message>
        <source>Failed to get Ip address!</source>
        <translation>Не вдалося отримати IP адресу.</translation>
    </message>
    <message>
        <source>Wireless Network</source>
        <translation>Безпровідні мережі</translation>
    </message>
    <message>
        <source>Failed to connect!</source>
        <translation>Не вдалось під&apos;єднатися до мережі.</translation>
    </message>
    <message>
        <source>Connecting .......</source>
        <translation>З&apos;єднання...</translation>
    </message>
</context>
</TS>
