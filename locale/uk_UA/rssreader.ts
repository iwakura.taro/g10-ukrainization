<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>RSFeedsWindow</name>
    <message>
        <source>Rss</source>
        <translation>RSS</translation>
    </message>
    <message>
        <source>Updating: %1, %2%</source>
        <translation>Оновлення: %1, %2% </translation>
    </message>
    <message>
        <source>Update subscription</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <source>second ago</source>
        <translation>Секунду назад</translation>
    </message>
    <message>
        <source>Updating: %1...</source>
        <translation>Оновлення: %1 ...</translation>
    </message>
    <message>
        <source>Update all subscription?</source>
        <translation>Оновити всі?</translation>
    </message>
    <message>
        <source>years ago</source>
        <translation>років тому</translation>
    </message>
    <message>
        <source>Update all</source>
        <translation>Оновити всі</translation>
    </message>
    <message>
        <source>days ago</source>
        <translation>днів тому</translation>
    </message>
    <message>
        <source>minutes ago</source>
        <translation>хвилин тому</translation>
    </message>
    <message>
        <source>months ago</source>
        <translation>місяців тому</translation>
    </message>
    <message>
        <source>Updating: %1</source>
        <translation>Оновлення: %1</translation>
    </message>
    <message>
        <source>History data more than 200M, remove it?</source>
        <translation>Історія займає більше 200Mб, очистити?</translation>
    </message>
    <message>
        <source>Question</source>
        <translation>RSS</translation>
    </message>
    <message>
        <source>hours ago</source>
        <translation>годин тому</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Увага</translation>
    </message>
</context>
<context>
    <name>RSMenu</name>
    <message>
        <source>Menu</source>
        <translation>меню</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>The file is not an XML version 1.0 file.</source>
        <translation>Формат файлу не XML версії 1.0.</translation>
    </message>
</context>
<context>
    <name>RCategoryWindow</name>
    <message>
        <source>add subscription</source>
        <translation>Додати</translation>
    </message>
    <message>
        <source>Import Subscription form local</source>
        <translation>Імпорт RSS</translation>
    </message>
    <message>
        <source>Import Text</source>
        <translation>Імпорт тексту</translation>
    </message>
    <message>
        <source>Please enter your RSS informations</source>
        <translation>Будь ласка, введіть RSS</translation>
    </message>
    <message>
        <source>Manually add RSS</source>
        <translation>Додати RSS</translation>
    </message>
    <message>
        <source>Import Subscription form feedlist.txt?</source>
        <translation>Імпорт з файлу feedlist.txt</translation>
    </message>
    <message>
        <source>Add Subscription</source>
        <translation>Додати</translation>
    </message>
</context>
<context>
    <name>QRefreshWindow</name>
    <message>
        <source>years ago</source>
        <translation>років тому</translation>
    </message>
    <message>
        <source>days ago</source>
        <translation>днів тому</translation>
    </message>
    <message>
        <source>minutes ago</source>
        <translation>хвилин тому</translation>
    </message>
    <message>
        <source>months ago</source>
        <translation>місяців тому</translation>
    </message>
    <message>
        <source>hours ago</source>
        <translation>годин тому</translation>
    </message>
</context>
<context>
    <name>RChannelWindow</name>
    <message>
        <source>Add Subscription</source>
        <translation>Додати</translation>
    </message>
</context>
</TS>
