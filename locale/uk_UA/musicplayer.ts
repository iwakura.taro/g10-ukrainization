<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>boeyemenuDlg</name>
    <message>
        <source>Play mode: loop</source>
        <translation>Повторювати все</translation>
    </message>
    <message>
        <source>Play mode: single</source>
        <translation>Повторювати</translation>
    </message>
    <message>
        <source>Play mode: random</source>
        <translation>Відтворювати вперемішку</translation>
    </message>
    <message>
        <source>Play mode: order</source>
        <translation>Відтворення по-порядку</translation>
    </message>
</context>
<context>
    <name>boeyeplayerui</name>
    <message>
        <source>You should close radio firstly.</source>
        <translation>Будь ласка, вимкніть радіо</translation>
    </message>
    <message>
        <source>Please insert earphone at first.</source>
        <translation>Будь ласка, приєднайте навушники</translation>
    </message>
    <message>
        <source>You should close TTS firstly.</source>
        <translation>Будь ласка, вимкніть читання </translation>
    </message>
    <message>
        <source>warning</source>
        <translation>Увага</translation>
    </message>
</context>
</TS>
