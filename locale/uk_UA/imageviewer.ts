<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>component::ImageWindow</name>
    <message>
        <source>Quit</source>
        <translation>Вихід</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Повноекранний режим</translation>
    </message>
    <message>
        <source>UnFullScreen</source>
        <translation>З інформацією про зображення</translation>
    </message>
    <message>
        <source>Rotate</source>
        <translation>Поворот зображення</translation>
    </message>
    <message>
        <source>Rotate 0</source>
        <translation>Без повороту</translation>
    </message>
    <message>
        <source>Keep rotation</source>
        <translation>З поворотом</translation>
    </message>
    <message>
        <source>Unkeep rotation</source>
        <translation>Дозволити поворот</translation>
    </message>
    <message>
        <source>Rotate 180</source>
        <translation>Протилежно</translation>
    </message>
    <message>
        <source>Rotate 270</source>
        <translation>Поворот проти годинникової стрілки</translation>
    </message>
    <message>
        <source>Rotate 90</source>
        <translation>Поворот за годинниковою стрілкою</translation>
    </message>
    <message>
        <source>Goto Page</source>
        <translation>Перейти на сторінку</translation>
    </message>
    <message>
        <source>Set screensaver success.</source>
        <translation>Заставку встановлено.</translation>
    </message>
    <message>
        <source>Set screensaver</source>
        <translation>Встановити заставку</translation>
    </message>
    <message>
        <source>Unset screensaver</source>
        <translation>Видалити заставку</translation>
    </message>
    <message>
        <source>Slideshow</source>
        <translation>Режим гортання</translation>
    </message>
</context>
</TS>
