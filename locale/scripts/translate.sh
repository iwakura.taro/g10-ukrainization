#!/bin/sh

LCONVERT=/usr/local/bin/lconvert
if [ ! -x "$LCONVERT" ]; then
  LCONVERT=/usr/lib/x86_64-linux-gnu/qt4/bin/lconvert
fi

TSES=`find . -name "*.ts"`
for TS in $TSES; do
  QM=`echo $TS | sed 's/ts$/qm/'`
  $LCONVERT $TS -o $QM
done

