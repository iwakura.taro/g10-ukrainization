#!/bin/sh

LCONVERT=/usr/local/bin/lconvert
if [ ! -x "$LCONVERT" ]; then
  LCONVERT=/usr/lib/x86_64-linux-gnu/qt4/bin/lconvert
fi

QMS=`find . -name "*.qm"`
for QM in $QMS; do
  TS=`echo $QM | sed 's/qm$/ts/'`
  if [ -f $TS ]; then
    $LCONVERT $QM -o $TS
  fi
done
